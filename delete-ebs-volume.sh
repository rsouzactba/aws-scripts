#!/bin/sh

##################################################################
# Delete EBS volumes                                             #
# by Ricardo Souza - @rsouzactba (19-10-2015)                    #
# recommendations:                                               #
# 1) Install and configure AWS Cli (aws-cli/1.15.83)			 #
# 2) Create folder /var/log/ec2					 				 #
##################################################################

## region (sa-east-1 = SP, us-east-1= Virginia, us-west-2= Oregon)
REGION_SP="sa-east-1"
REGION_VIRGINIA="us-east-1"
REGION_OREGON="us-west-2"
DATA=`/bin/date +%d"/"%m"/"%Y"-"%H":"%M"(UTC)"`
ARQ_LOG="/var/log/ec2/volumes_deleted.log"
AWS=`which aws`

check_volumes(){
#get region
REGION=$1
#check if has or not volume to delete "AVAILABLE" state
VOLUMES=`$AWS ec2 describe-volumes --region $REGION |awk '{printf $0}'| sed 's/ //g'|sed 's/{"AvailabilityZone"/\n{"AvailabilityZone"}/g' |sed 's/|/\n/g' |grep "available"`
        if [ "$VOLUMES" != ""  ]
        then
            for i in $VOLUMES
            do
                VOLUME_ID=`echo $i| awk -F'"' '{print $14}'`
                /usr/bin/aws ec2 delete-volume --volume-id $VOLUME_ID --region $REGION > /dev/null 2>&1
                echo "Volume deleted: $VOLUME_ID - Region: $REGION - $DATA "  >> $ARQ_LOG
            done
          else
            echo "There's no volume to delete in $REGION - $DATA" >> $ARQ_LOG
        fi
        echo "------------------------------------------------------" >> $ARQ_LOG
}

## Call function to check_volume
check_volumes $REGION_VIRGINIA
check_volumes $REGION_OREGON

exit

