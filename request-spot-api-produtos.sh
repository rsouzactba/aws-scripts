#!/bin/bash

############################################################################################
# request for spot instance and set flags - by Ricardo Souza - 17/10/2018                  # 
# requirements: aws cli configured with permission to create ec2 and spot request          #
# this example cover:									   #
# 1) ec2 t2.medium; 2) Amazon Linux							   #
############################################################################################

#your aws (this example = Virginia)
region="us-east-1"
zone="us-east-1c"
key="Your Key"
ami_id="You AMI"
sg="sg-your-security-group"
subnet="subnet-XXXX"
#### ondemand price 
price_ondemand="0.046400"
instance_type="t2.medium"
name_ec2="Name_Your_Instance"
project="api-produtos"
####
request_spot_file="/tmp/request_spot_file.json"


#################### function get spot price ###########################
get_price(){

#date parameters
days_ago=`date -d "3 day ago" '+%Y-%m-%dT00:00:00'`
yesterday=`date -d "1 day ago" '+%Y-%m-%dT00:00:00'`

#get price last days
price_spot=`aws ec2 describe-spot-price-history --start-time $days_ago --end-time $yesterday --instance-type $instance_type --availability-zone $zone --product-description "Linux/UNIX (Amazon VPC)" | jq '.SpotPriceHistory[].SpotPrice'|sed -e 's/"//g'|sort -nr|head -1`

#additional price to get instance
bet="0.009"
#calculate price (price_spot + bet)
price_tmp=`echo $price_spot $bet|awk '{ print $1 + $2 }'`

if [ $(echo "$price_tmp>$price_ondemand" |bc) -eq 1 ];then
	price=$price_ondemand
     else
	price=$price_tmp
fi
}

########## function set tags in spot request and ec2 instance ##########
config_tags(){

echo "waiting aws response for request id spot...."
sleep 30
echo "setup tags in request spot was conclude successfully."
#get request spot id
requestId=$(cat $request_spot_file|jq '.SpotInstanceRequests[].SpotInstanceRequestId'|sed -e 's/"//g')

#set tags in request spot
$(aws ec2 create-tags --resources "$requestId" --tags Key=Name,Value=$name_ec2 --region $region)

echo "waiting aws response for creating ec2 instance...."
while true;do
	#get id ec2 instance
	instanceId=$(aws ec2 describe-spot-instance-requests --spot-instance-request-ids $requestId |jq '.SpotInstanceRequests[].InstanceId'|sed -e 's/"//g')
	if [ "$instanceId" != "" ];then
		#set tags to ec2 instance
		aws ec2 create-tags --resources "$instanceId" --tags Key=Name,Value=$name_ec2-SPOT-C Key=application,Value=$project Key=env,Value=production --region $region
		if [ $? -eq 0  ];then
			echo "setup tags in ec2 instance spot  was conclude successfully."
			#delete tmp file
			rm -f $request_spot_file > /dev/null
			break
		   else
		     	echo "ops!! we have a problem!!"
		fi
        fi
done
}


########### function request spot #######################
request_spot(){

#get price spot
get_price

#do request spot 
aws ec2 request-spot-instances --region $region --spot-price $price --type "one-time" --instance-interruption-behavior "terminate"  --launch-specification "
{ 
	\"KeyName\": \"$key\", 
	\"ImageId\": \"$ami_id\" , 
	\"InstanceType\": \"$instance_type\" ,  
	\"Placement\": {	
		\"AvailabilityZone\": \"$zone\"
	}, 
	\"NetworkInterfaces\": 
	[	{	
		\"DeviceIndex\": 0, 
		\"SubnetId\": \"$subnet\",
		\"Groups\": [ \"$sg\" ],
		\"AssociatePublicIpAddress\": false
	}]
}" > $request_spot_file
echo "building request spot in aws...."
config_tags
}

request_spot


