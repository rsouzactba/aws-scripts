#!/bin/sh

##################################################################
# Generate a AMI from instance id                                #
# br Ricardo Souza - @rsouzactba (19-10-2015)                    #
##################################################################

#REGION_SP="sa-east-1"
REGION="us-east-1"
DATA=`/bin/date +%d"-"%m"-"%Y`

instance_id=$1
instance_name="name-your-new-ami"

if [ "$instance_id" != "" ];then
	echo "Generation your AMI..."
	aws ec2 create-image --instance-id $instance_id --name $DATA-$instance_name --description $instance_name --no-reboot --region $REGION
	if [ $? -eq 0 ];then
		echo "It's all right!"
	   else
		echo "Ops... Houston we have a problem, here!! :("
	fi	
    else
	echo "Hey, it's necessary instance id!"
fi


exit
