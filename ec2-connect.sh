#!/bin/sh

#################################################################################################
# script to access your ec2 instance via ssh                                                    #
# requirements:                                                                                 #   
# 1) install jq and aws cli (configured)                                                        #
# 2) copy your Key pair name to folder /home/$user/.aws/keys with same name used in AWS Console #
# 3) jq installed			            					        #
# 4) aws credential with permission to list EC2 instances ("allow" - "ec2:Describe")            #
# Default ssh account: ec2-user				                                        #
# by Ricardo Souza - rsouzactba@hotmail.com | v1 - nov/2018				        #
#################################################################################################

#get your user name
user=`whoami`
file_ec2_conect="/home/$user/.aws/file_ec2_conect.txt"
path_keys="/home/$user/.aws/keys"
file_tmp="/tmp/list-ec2.tmp"
AWS=`which aws`
#lista_ec2=`$AWS ec2 describe-instances --filter "Name=tag-key,Values=Name" "Name=tag-value,Values=*$name_tag*" "Name=instance-state-name,Values=running" --query "Reservations[*].Instances[*][KeyName,Tags[?Key=='Name'].Value[],NetworkInterfaces[0].PrivateIpAddresses[0].PrivateIpAddress]"  --output text |sed 'N;s/\n/ /'|awk '{print $3"|"$2"|"$1}'|sort -h`

#get list ec2 instances to output file
$($AWS ec2 describe-instances --filter "Name=tag-key,Values=Name" "Name=tag-value,Values=*$name_tag*" "Name=instance-state-name,Values=running" --query "Reservations[*].Instances[*][KeyName,Tags[?Key=='Name'].Value[],NetworkInterfaces[0].PrivateIpAddresses[0].PrivateIpAddress]"| jq '.'|grep -v -e '[][]' |awk '{ printf "%s", $0; if (NR % 3 == 0) print ""; else printf "|" }'|sed 's/"//g'|sed -e 's/,//g' | sed -e 's/ //g' |sort -h > $file_tmp)

#print head of script 
echo 
echo "\033[0;34m    List Instances  \033[0m"
echo "\033[0;34m====================\033[0m"
echo "Choose your EC2 instance to connect:" 

counter="1"
for ec2 in `cat $file_tmp`;
do 
	name_ec2=`echo $ec2|awk -F"|" '{print ""$2}'`
	ip_ec2=`echo $ec2|awk -F"|" '{print $3}'`
	chave_ec2=`echo $ec2| awk -F"|" '{print $1}'`
	echo "\033[0;34m$counter\033[0m) "$name_ec2 " - "$ip_ec2 
	echo "$counter,$name_ec2,$ip_ec2,$chave_ec2" >> $file_ec2_conect
        counter=` expr $counter + 1 `
done

echo
read -p "Select number of instance to connect: " ec2_num 

#filter ec2
ec2_to_connect=`grep ^"$ec2_num," $file_ec2_conect`
#mount line command to connect
connect=`echo $ec2_to_connect | awk '{print $1}'|awk -v path_keys="$path_keys" -F"," '{print "ssh ec2-user@"$3" -i "path_keys"/"$4".pem"}'`

#call command line to connect
$connect 

#del tmp files
rm -f $file_ec2_conect $file_tmp

exit
